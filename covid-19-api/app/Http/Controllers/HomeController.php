<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    //Retorna view padrão(Home) do laravel
    public function index()
    {
        return view('home');
    }

    public function getAPICasos()
    {
        $pais = 'Brazil';

        $api = 'https://covid-api.mmediagroup.fr/v1/cases?country=' . $pais;

        $response = Http::get($api);

        $array = $response["All"];

        return response()->json([
            'Casos Confirmados' => $array['confirmed'],
            
        ], 200);
       
    }

    public function getAPIVacinas()
    {
        $pais = 'Brazil';

        $api = 'https://covid-api.mmediagroup.fr/v1/vaccines?country=' . $pais;

        $response = Http::get($api);

        $array = $response["All"];

        return response()->json([
            'Pessoas Vacinadas' => $array['people_vaccinated'],
            'Pessoas Parcialmente Vacinadas' => $array['people_partially_vaccinated'],
            
        ], 200);
    }

    public function getAPIHistorico()
    {
        $pais = 'Brazil';

        $api = 'https://covid-api.mmediagroup.fr/v1/history?country=' . $pais . '&status=Confirmed';

        $response = Http::get($api);

        $array = $response["All"];

        return response()->json([
            '2020' => $array['dates'],
            
        ], 200);
    }
}
