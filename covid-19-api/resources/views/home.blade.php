<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>API Covid-19 - Tec4You</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    </head>
    <body>
        <div class="section" id="graficos">
            <div class="container">
                <div class="mt-2">
                    <div class="row">
                        <!-- Card para gráfico do número de vacinas aplicadas -->
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center mt-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Gráfico para vacinas de Covid-19 no Brasil</h4>
                                </div>
                                <div class="card-body">
                                    <canvas id="graficoApiVacinas" width="300" height="300"></canvas>
                                </div>
                              </div>
                        </div>
                        <!-- Card para gráfico de casos confirmados -->
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center mt-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Gráfico para casos de Covid-19 no Brasil</h4>
                                </div>
                                <div class="card-body">
                                    <canvas id="graficoApi" width="300" height="300"></canvas>
                                </div>
                              </div>
                        </div>
                        <!-- Card para gráfico do historico do país -->
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center mt-4 mb-5">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Gráfico para histórico de Covid-19 no Brasil</h4>
                                </div>
                                <div class="card-body">
                                    <canvas id="graficoApiHistorico" width="300" height="300"></canvas>
                                </div>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.5.1/dist/chart.min.js"></script>

    <script>

        getApiVacinas('Brazil'); //Inicia o Grafico 02 (Vacinas)
        getApiCasos('Brazil'); //Inicia o Grafico 03 (Casos)

            //Grafico 01
            var graficoctx = document.getElementById('graficoApi').getContext('2d');
            var graficosChart = new Chart(graficoctx);   
            //Grafico 02
            var graficoVacinasctx = document.getElementById('graficoApiVacinas').getContext('2d');
            var graficosVacinasChart = new Chart(graficoVacinasctx);   
        

            //Função Ajax para chamada do gráfico 02
            function getApiVacinas(date){

                $.ajax({
                    url: '/grafico-api-covid/vacinas/' + date,
                    dataType: 'json',
                    success: function(response){
                        var labels = Object.keys(response);
                        var data = Object.values(response);
                        criarGraficoAPIVacinas(labels, data);
                    }
                })
            }

            //Função Ajax para chamada do gráfico 03
            function getApiCasos(date){

                $.ajax({
                    url: '/grafico-api-covid/' + date,
                    dataType: 'json',
                    success: function(response){
                        var labels = Object.keys(response);
                        var data = Object.values(response);
                        criarGraficoAPICasos(labels, data);
                    }
                })
            }

            //Função para iniciar o gráfico 02
            function criarGraficoAPIVacinas(labels, data){
                
                graficosVacinasChart.destroy();
                graficosVacinasChart = new Chart(graficoVacinasctx, {
                    type: 'doughnut',
                    data: {
                        labels: labels,
                        datasets: [{
                            label: 'Brasil' ,
                            data: data,
                            backgroundColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(255, 206, 86, 1)',
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(255, 206, 86, 1)',
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    }
                });
            }
            //Função para iniciar o gráfico 03
            function criarGraficoAPICasos(labels, data){
                graficosChart.destroy();
                graficosChart = new Chart(graficoctx, {
                    type: 'bar',
                    data: {
                        labels: labels,
                        datasets: [{
                            label: 'Brasil' ,
                            data: data,
                            backgroundColor: [
                                'rgba(54, 162, 235, 1)'
                            ],
                            borderColor: [
                                'rgba(54, 162, 235, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    }
                });
            }
        </script>
</html>
