<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('dashboard');

Route::get('/grafico-api-covid/{pais_localidade}', 'HomeController@getAPICasos')->name('api.get'); //Recupera os dados da API de Casos
Route::get('/grafico-api-covid/vacinas/{pais_localidade}', 'HomeController@getAPIVacinas')->name('api.get'); //Recupera os dados da API de Vacinas
Route::get('/grafico-api-covid/historico/{pais_localidade}', 'HomeController@getAPIHistorico')->name('api.get'); //Recupera os dados da API de Historico